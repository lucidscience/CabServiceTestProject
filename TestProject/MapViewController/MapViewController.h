//
//  MapViewController.h
//  TestProject
//
//  Created by Muhammad Mashood Tanveer on 25/09/2017.
//  Copyright © 2017 Muhammad Mashood Tanveer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MapViewController : UIViewController

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end
