//
//  MapViewController.m
//  TestProject
//
//  Created by Muhammad Mashood Tanveer on 25/09/2017.
//  Copyright © 2017 Muhammad Mashood Tanveer. All rights reserved.
//

#import "MapViewController.h"
#import "TestProject-Swift.h"

@interface MapViewController ()
@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Setup LocationManger
    MKCoordinateRegion region;
    region.center.latitude = 48.137154;
    region.center.longitude = 11.616124;
    region.span.latitudeDelta = 0.04;
    region.span.longitudeDelta = 0.04;
    [self.mapView setRegion:region animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//Override MapView Delegate didSelectAnnotationView to set Annotation Title
-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    if([view viewWithTag:756]) {
        [[view viewWithTag:756] removeFromSuperview];
    }
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, -10, 70, 20)];
    titleLabel.text = view.annotation.title;
    titleLabel.tag = 756;
    [view addSubview:titleLabel];
    [mapView selectAnnotation:view.annotation animated:YES];
}

//Override MapView Delegate mapViewDidFinishRenderingMap to fetch nearby Taxi's through API call
- (void)mapViewDidFinishRenderingMap:(MKMapView *)mapView fullyRendered:(BOOL)fullyRendered {
    [Vehicle fetchVehiclesWithCompletion:^(NSArray *vehicles) {
        [mapView removeAnnotations:mapView.annotations];
        if(vehicles.count > 0) {
            for (Vehicle *veh in vehicles) {
                MKPointAnnotation* annotation= [MKPointAnnotation new];
                annotation.coordinate = veh.location;
                annotation.title = veh.type;
                [mapView addAnnotation: annotation];
            }
        }
    }];
}

@end
