//
//  TableViewCell.swift
//  TestProject
//
//  Created by Muhammad Mashood Tanveer on 25/09/2017.
//  Copyright © 2017 Muhammad Mashood Tanveer. All rights reserved.
//

import Foundation
import UIKit

// Extended Double to add truncate function that will keep only the places specified in the argument
extension Double
{
    func truncate(places : Int)-> Double
    {
        return Double(floor(pow(10.0, Double(places)) * self)/pow(10.0, Double(places)))
    }
}

//Custom Table View Cell
class TableViewCell: UITableViewCell {
    @IBOutlet weak var vehicleType: UILabel!
    @IBOutlet weak var vehicleDistance: UILabel!
    @IBOutlet weak var vehicleStatus: UIImageView!
}

//Table View Controller class with the table view delegates and datasources
class TableViewController: UITableViewController {
    var vehicles: [Vehicle] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        Vehicle.fetchVehicles() {
            vehicles in
            self.vehicles = vehicles
            self.tableView.reloadData()
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vehicles.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as! TableViewCell
        let vehicle = self.vehicles[indexPath.row]
        cell.vehicleType.text = vehicle.type
        cell.vehicleDistance.text = "\(vehicle.heading.truncate(places: 2)) m"
        if vehicle.isActive {
            cell.vehicleStatus.image = UIImage(named: "taxiActiveIcon")
        } else {
            cell.vehicleStatus.image = UIImage(named: "taxiInActiveIcon")
        }
        cell.selectionStyle = .none
        return cell
    }
    
}
