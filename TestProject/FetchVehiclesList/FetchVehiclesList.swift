//
//  FetchVehiclesList.swift
//  TestProject
//
//  Created by Muhammad Mashood Tanveer on 25/09/2017.
//  Copyright © 2017 Muhammad Mashood Tanveer. All rights reserved.
//

import Foundation
import Alamofire

@objc class Vehicle: NSObject {
    // Basic Vehicle Object
    @objc var location: CLLocationCoordinate2D
    @objc var isActive: Bool
    @objc var type: String
    @objc var heading: Double
    
    // Data Parse Logic
    enum SerializationError: Error {
        case missing(String)
    }
    init(json: [String: Any]) throws {
        //Extract Vehicle Identifier
        
        guard let coordinatesJson = json["coordinate"] as? [String: Double],
            let latitude = coordinatesJson["latitude"],
            let longitude = coordinatesJson["longitude"]
            else {
                throw SerializationError.missing("coordinate")
        }
        guard let state = json["state"] as? String else {
            throw SerializationError.missing("state")
        }
        var isActive = false
        if state == "ACTIVE" {
            isActive = true
        }
        
        guard let type = json["type"] as? String else {
            throw SerializationError.missing("type")
        }
        
        guard let heading = json["distance"] as? Double else {
            throw SerializationError.missing("distance")
        }
        self.location = CLLocationCoordinate2D()
        self.location.latitude = latitude
        self.location.longitude = longitude
        self.isActive = isActive
        self.type = type.capitalized
        self.heading = heading
    }
}

//Extension with static function to fetch vehicles through API call
extension Vehicle {
    static func fetchVehicles(completion: @escaping ([Vehicle]) -> Void) {
        let dummyJsonUrlStr = "https://my-json-server.typicode.com/mashoodtanveer/test_files/db"
        var vehicles: [Vehicle] = []
        Alamofire.request(dummyJsonUrlStr).responseJSON { response in
            if let jsonData = response.data {
                if let json = try? JSONSerialization.jsonObject(with: jsonData, options: []) as! [String: Any] {
                    if let vehiclesArray = json["poiList"] as? [[String:Any]] {
                        for case let vehicleJson in vehiclesArray {
                            if let vehicle = try? Vehicle(json: vehicleJson) {
                                vehicles.append(vehicle)
                            }
                        }
                    }
                }
            }
            completion(vehicles)
        }
    }
}
